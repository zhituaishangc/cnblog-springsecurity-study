package com.lyy.controller;

import com.lyy.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/res")
@RestController
public class ResourcesController {

    @Autowired
    private ResourceService resourceService;

    @GetMapping(value = "/r1",produces = {"text/plain;charset=UTF-8"})
    public String res1(){
        return resourceService.res1();
    }

    @GetMapping(value = "/r2",produces = {"text/plain;charset=UTF-8"})
    public String res2(){
        return resourceService.res2();
    }

    @GetMapping(value = "/r3",produces = {"text/plain;charset=UTF-8"})
    public String res3(){
        return resourceService.res3();
    }

    @GetMapping(value = "/r4",produces = {"text/plain;charset=UTF-8"})
    public String res4(){
        return resourceService.res4();
    }

    @GetMapping(value = "/r5",produces = {"text/plain;charset=UTF-8"})
    public String res5(){
        return resourceService.res5();
    }
}
