package com.lyy.service;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ResourceService {

    //这个方法允许匿名访问
    @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
    String res1();

    //这个方法要有p1角色才能访问

    @Secured("ROLE_P1")
    String res2();

    //这个方法要有P1或p2角色才能访问
    @Secured({"ROLE_P1","ROLE_P2"})
    String res3();

    //匿名访问
    @PreAuthorize("isAnonymous()")
    String res4();

    //拥有角色1，角色名要带ROLE_前缀
    @PreAuthorize("hasRole('ROLE_P1')")
    String res5();
}
