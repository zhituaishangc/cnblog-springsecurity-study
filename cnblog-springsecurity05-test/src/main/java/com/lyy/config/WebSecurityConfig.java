package com.lyy.config;

import com.lyy.security.NoAccessDeniedHandler;
import com.lyy.security.UnLoginHandler;
import com.lyy.security.filter.TokenVerifyFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 配置springsecurity
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    //用户配置,
    @Bean
    public UserDetailsService userDetailsService(){
        //在内存中配置用户
        InMemoryUserDetailsManager manager=new InMemoryUserDetailsManager();
        manager.createUser(User.withUsername("lyy").password("123").authorities("ROLE_P1").build());
        manager.createUser(User.withUsername("zs").password("456").authorities("ROLE_P2").build());
        return manager;
    }

    //配置自定义的对token进行验证的过滤器
    @Autowired
    private TokenVerifyFilter tokenVerifyFilter;

    //密码加密方式配置
    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    //安全配置
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //匹配路径时越具体的路径要先匹配
        http.authorizeRequests().antMatchers("/","/index.html").permitAll();
        //放行申请token的url
        http.authorizeRequests().antMatchers("/authenticate/**").permitAll();
        //需要p1权限才能访问
        http.authorizeRequests().antMatchers("/resource/r1").hasRole("P1");
        //需要p2权限才能访问
        http.authorizeRequests().antMatchers("/resource/r2").hasRole("P2")
        .antMatchers("/resource/r3").hasRole("P3");//需要p3权限才能访问
        http.authorizeRequests().anyRequest().authenticated();
        http.formLogin().disable();//禁用表单登录
        //设置未登录或登录失败时访问资源的处理方式
        http.exceptionHandling().authenticationEntryPoint(new UnLoginHandler());
        //设置权限不足,无法访问当前资源时的处理方式
        http.exceptionHandling().accessDeniedHandler(new NoAccessDeniedHandler());
        http.addFilterBefore(tokenVerifyFilter, UsernamePasswordAuthenticationFilter.class);
        //设置不使用session,无状态
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * 配置认证管理器:
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
