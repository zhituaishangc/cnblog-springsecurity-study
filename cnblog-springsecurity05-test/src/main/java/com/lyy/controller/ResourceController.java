package com.lyy.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resource")
public class ResourceController {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    @GetMapping("/r1")
    public JsonNode r1() {
        ObjectNode res = MAPPER.createObjectNode();
        res.put("code","200");
        res.put("data","访问资源1成功");
        return res;
    }

    @GetMapping("/r2")
    public JsonNode r2() {
        ObjectNode res = MAPPER.createObjectNode();
        res.put("code","200");
        res.put("data","访问资源2成功");
        return res;
    }

    @GetMapping("/r3")
    public JsonNode r3() {
        ObjectNode res = MAPPER.createObjectNode();
        res.put("code","200");
        res.put("data","访问资源3成功");
        return res;
    }
}
