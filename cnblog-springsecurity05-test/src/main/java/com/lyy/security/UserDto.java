package com.lyy.security;

import lombok.Data;

/**
 * 封装登录请求参数
 */
@Data
public class UserDto {
    private String username;
    private String password;
}
