package com.lyy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnblogSpringSecurity05Application {
    public static void main(String[] args) {
        SpringApplication.run(CnblogSpringSecurity05Application.class,args);
    }
}
