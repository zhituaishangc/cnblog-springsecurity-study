package com.lyy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnblogSpringSecurity01Application {
    public static void main(String[] args) {
        SpringApplication.run(CnblogSpringSecurity01Application.class,args);
    }
}
