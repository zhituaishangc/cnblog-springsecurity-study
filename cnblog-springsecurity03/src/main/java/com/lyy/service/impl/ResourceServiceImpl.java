package com.lyy.service.impl;

import com.lyy.service.ResourceService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class ResourceServiceImpl implements ResourceService {
    @Override
    public String res1() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="匿名用户";
        if(principal instanceof UserDetails){
            UserDetails userDetails= (UserDetails) principal;
            username=userDetails.getUsername();
        }

        return username+",访问资源1";
    }

    @Override
    public String res2() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="匿名用户";
        if(principal instanceof UserDetails){
            UserDetails userDetails= (UserDetails) principal;
            username=userDetails.getUsername();
        }

        return username+",访问资源2";
    }

    @Override
    public String res3() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="匿名用户";
        if(principal instanceof UserDetails){
            UserDetails userDetails= (UserDetails) principal;
            username=userDetails.getUsername();
        }

        return username+",访问资源3";
    }

    @Override
    public String res4() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="匿名用户";
        if(principal instanceof UserDetails){
            UserDetails userDetails= (UserDetails) principal;
            username=userDetails.getUsername();
        }

        return username+",访问资源4";
    }

    @Override
    public String res5() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="匿名用户";
        if(principal instanceof UserDetails){
            UserDetails userDetails= (UserDetails) principal;
            username=userDetails.getUsername();
        }

        return username+",访问资源5";
    }
}
