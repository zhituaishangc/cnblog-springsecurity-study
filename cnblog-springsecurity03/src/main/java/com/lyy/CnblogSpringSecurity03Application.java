package com.lyy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnblogSpringSecurity03Application {
    public static void main(String[] args) {
        SpringApplication.run(CnblogSpringSecurity03Application.class,args);
    }
}
